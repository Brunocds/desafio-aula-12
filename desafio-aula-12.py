quantidade_num = int(input('Quantos números deseja inserir?\n'))

list_nums = []
for i in range(quantidade_num):
    list_nums.append(float(input(f'Digite o {i+1}º número: ')))

media = sum(list_nums)/len(list_nums)

print(f'A média dos números inseridos é: {media}')
